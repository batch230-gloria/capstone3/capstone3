import './App.css';
// import React from 'react';
import { Fragment } from 'react';
import { Container } from 'react-bootstrap';
import { UserProvider } from './UserContext';
import { useState, useEffect, useContext } from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes } from 'react-router-dom';


import  OrderDashboard from './components/OrderDashboard';
import  UserDashboard from './components/UserDashboard';
import AdminDashboard from './pages/AdminDashboard';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Register from './pages/Register';
import Products from './pages/Products';
import ProductView from './pages/ProductView';
import Home from './pages/Home';
// import Order from './components/Order';
import Error from './pages/Error'; 
// import Navbar from './components/Navbar';
import Contact from './components/Contact';
import Cart from './pages/Cart'; 



function App() {

  const [user, setUser] = useState({
    id: localStorage.getItem('id')
  })

  const unsetUser = () => {
    localStorage.clear();
  }


  return (
    <UserProvider value = {{user, setUser, unsetUser}}>

      <Router>

        <Routes>
          <Route exact path="/" element={<Home />} />
          <Route exact path="/admin" element={<AdminDashboard />} />
          <Route exact path="/userDashboard" element={<UserDashboard />} />
          <Route exact path="/orderDashboard" element={<OrderDashboard />} />
          <Route exact path="/products" element={<Products />} />
          <Route exact path="/products/:productId" element={<ProductView />} />
          <Route exact path="/contacts" element={<Contact />} />
          <Route exact path="/login" element={<Login />} />
          <Route exact path="/logout" element={<Logout />} /> 
          <Route exact path="/register" element={<Register />} />
          <Route exact path="/order" element={<Cart />} />
          <Route exact path="*" element={<Error />} /> 

        </Routes>
      </Router>

    </ UserProvider>

  );
}

export default App;
