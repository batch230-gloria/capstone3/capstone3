import { useEffect, useState, useContext } from "react";
import { Navigate, Link } from "react-router-dom";
import style from './modules/Product.module.css';
import ProductCard from "../components/ProductCard";
import UserContext from "../UserContext";
import Footer from '../components/Footer' ;
import { Fragment } from 'react';
import "../components/modules/Navbar.css";



export default function Products() {

	const { user, setUser } = useContext(UserContext);
	const [isMobile, setIsMobile] = useState(false);
 	const [userId, setUserId] = useState(localStorage.getItem("userId"));


	const [products, setProducts] = useState([]);

	useEffect(() =>{
		// Will retrieve all the active products
		fetch(`${process.env.REACT_APP_API_URL}/products/active`)
		.then(res => res.json())
		.then(data => {
			console.log(data);
			setProducts(data.map(product =>{
				return(
					
					<ProductCard key={product._id} productProp={product}/>
				);
			}));
		})
	}, []);



	return(
		(user.isAdmin)
		?
			<Navigate to="/admin" />
		:	
		<>	
		<Fragment>
			<div className={style.whole}>

				<nav className="navbar">
				  <Link to="/" className="home">
				  <h3 className="logo">Logo</h3>
				  </Link>
				  <ul className={isMobile? "nav-links-mobile" : "nav-links"}
				  onClick={() => setIsMobile(false)}
				  >
				    <Link to="/" className="home">
				      <li>Home</li>
				    </Link>

				    <Link to="/contacts" className="products">
				      <li>Contact</li>
				    </Link>

				    <Link to="/products" className="products">
				      <li>Products</li>
				    </Link>

				    {(user.id === null && userId === null)?
				    <>
				    <Link to="/register" className="products">
				      <li>Sign Up</li>
				    </Link>

				    <Link to="/login" className="signup">
				      <li>Sign In</li>
				    </Link>
				    </>
				    :
				    <>
				    <Link to="/order" className="products">
				      <li>Orders</li>
				    </Link>

				    <Link to="/logout" className="signup">
				      <li>Sign Out</li>
				    </Link>
				    </>
				    }

				    
				  </ul>
				  <button 
				  className="mobile-menu-icon"
				  onClick={() => setIsMobile(!isMobile)}
				  >
				    {isMobile ? 
				    <i className="fas fa-times"></i> 
				    : 
				    <i className="fas fa-bars"></i>}
				  </button>
				</nav>

				<div className={style.container}>
					<h2 className="text-center pt-5">Explore Our Fruits</h2>
					<div class={style.wrapper}>
						{products}
					</div>
				</div>
			</div>
			<Footer/>
		</Fragment>	
		</>
	)
}