import {useContext, useEffect, useState} from "react";
import {Navigate, NavLink, Link} from "react-router-dom";
import UserContext from "../UserContext";
import Footer from '../components/Footer' ;
import { Fragment } from 'react';
import "../components/modules/Navbar.css";
import styled from "styled-components";

const Container = styled.div`
  background-color: #00477b;
  height:100%;
`;

const OrderContainer = styled.div`
`;

const Wrapper = styled.div`
  padding: 20px;
 @media (max-width: 820px) {
    padding: 10px;
  }
`;

const Title = styled.h1`
  font-weight: 300;
  text-align: center;
  color:#fff;
`;

const Top = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 20px;
`;

const TopButton = styled.button`
  padding: 10px;
  font-weight: 600;
  cursor: pointer;
  border: black;
  background-color: black;
  color: #fff;
`;


const Bottom = styled.div`
  display: flex;
  justify-content: space-between;
  @media (max-width: 820px) {
    flex-direction: column;
  }

`;

const Info = styled.div`
  flex: 1;
`;

const Product = styled.div`
  display: flex;
  padding-left: 400px;
  @media (max-width: 820px) {
    flex-direction: column;
    padding-left: 0px;
  }
`;

const ProductDetail = styled.div`
  flex: 2;
  display: flex;
  align-items: center;
  justify-content: center;
  color:#fff;
  @media (max-width: 820px) {
    flex-wrap: wrap;
  }
`;

const Image = styled.img`
  width: 200px;
  height: 200px;
  object-fit: cover;

`;

const Details = styled.div`
  padding: 20px;
  display: flex;
  flex-direction: column;
  justify-content: space-around;
`;

const ProductName = styled.span``;

const ProductId = styled.span``;

const ProductColor = styled.div`
  width: 20px;
  height: 20px;
  border-radius: 50%;
  background-color: #fff;
`;

const ProductSize = styled.span`
  display: flex;
  flex-direction: column;
  @media (max-width: 820px) {
    flex-direction: row;
    gap: 20px;
  }
`;

const PriceDetail = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  padding-left: 0;

`;

const ProductPrice = styled.div`
  font-size: 30px;
  font-weight: 200;
  @media (max-width: 820px) {
    margin-top: 55px;
    font-size: 20px;
  }
`;

const Hr = styled.hr`
  background-color: #eee;
  border: none;
  height: 1px;
`;

const Cart = () => {

  const [isMobile, setIsMobile] = useState(false);
  const [userId, setUserId] = useState(localStorage.getItem("userId"));

  const { user, setUser } = useContext(UserContext);

  const [allUsers, setAllUsers] = useState([]);

  const fetchData = () =>{
    fetch(`${process.env.REACT_APP_API_URL}/users/orders`, {
      headers:{
        "Authorization": `Bearer ${localStorage.getItem("token")}`
      }
    })
    .then(res => res.json())
    .then(data => {
      console.log(data);

      setAllUsers(data.map(user => {
        return (
           
          <Product>
            <ProductDetail>
              <Image src={user.image}/>
              <Details>
                <ProductName>
                  <b>Product:</b> {user.name}
                </ProductName>
                <ProductId>
                  <b>ID:</b> {user.productId}
                </ProductId>
                <ProductSize>
                  <b>x{user.quantity}</b>
                </ProductSize>
              </Details>
            
            <PriceDetail>
              
              <ProductPrice>Total: ₱ {user.totalAmount}</ProductPrice>
            </PriceDetail>

            </ProductDetail>
            <Hr />
          </Product>
        )
      }));
    });
  }

  useEffect(()=>{
    fetchData();
  }, [])


  return (
    <Fragment>

      <Container>
        <nav className="navbar">
          <Link to="/" className="home">
          <h3 className="logo">Logo</h3>
          </Link>
          <ul className={isMobile? "nav-links-mobile" : "nav-links"}
          onClick={() => setIsMobile(false)}
          >
            <Link to="/" className="home">
              <li>Home</li>
            </Link>

            <Link to="/contacts" className="products">
              <li>Contact</li>
            </Link>

            <Link to="/products" className="products">
              <li>Products</li>
            </Link>

            {(user.id === null && userId === null)?
            <>
            <Link to="/register" className="products">
              <li>Sign Up</li>
            </Link>

            <Link to="/login" className="signup">
              <li>Sign In</li>
            </Link>
            </>
            :
            <>
            <Link to="/order" className="products">
              <li>Orders</li>
            </Link>

            <Link to="/logout" className="signup">
              <li>Sign Out</li>
            </Link>
            </>
            }

            
          </ul>
          <button 
          className="mobile-menu-icon"
          onClick={() => setIsMobile(!isMobile)}
          >
            {isMobile ? 
            <i className="fas fa-times"></i> 
            : 
            <i className="fas fa-bars"></i>}
          </button>
        </nav>

        <OrderContainer>
          <Wrapper>
            <Title>YOUR ORDER</Title>
            <Top>
              <TopButton>CONTINUE SHOPPING</TopButton>
            </Top>
            <Bottom>
              <Info>
                {allUsers}
                
              </Info>
              
            </Bottom>
          </Wrapper>

        </OrderContainer>
      </Container>  
      <Footer/>
    </Fragment>
  );
};

export default Cart;
