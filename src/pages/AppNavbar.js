import {Container, Navbar, Nav} from 'react-bootstrap'
import {Link, NavLink} from 'react-router-dom';
import {useState, Fragment, useContext} from 'react'; //
import UserContext from "../UserContext";
import style from './modules/AppNavbar.module.css';

export default function AppNavbar(){

	const { user, setUser } = useContext(UserContext);

	const [userId, setUserId] = useState(localStorage.getItem("userId"));

	return(
		
		<Navbar className={style.navbarContainer} expand="lg">
		    <Container>
		        <Navbar.Brand as={Link} to="/" className="brand">Fruits</Navbar.Brand>
		        <Navbar.Toggle  aria-controls="basic-navbar-nav"/>
		        <Navbar.Collapse  id="basic-navbar-nav">
					
					<div className={style.div}>
						<Nav>
							<NavLink className={style.textLink} to="/">
								<h6 className={style.list}>Home</h6>
							</NavLink>
							
							<NavLink className={style.textLink} to="/products">
								<h6 className={style.list}>Products</h6>
							</NavLink>

							{/*<NavLink className={style.textLink} to="/register">
								<h6 className={style.list}>Register</h6>
							</NavLink>*/}

						</Nav>
		            </div>
		            {(user.id === null && userId === null)?
		           	<div className={style.buttonDiv}>
		           		
						<NavLink to="/register">
							<button className={style.btn}>Register</button>
						</NavLink>
						<NavLink to="/login">
							<button className={style.btn}>Login</button>
						</NavLink>
					</div>
					:
					<div className={style.buttonDiv}>
						<NavLink to="/order">
							<button className={style.btn}><i class="fa-solid fa-truck fa-2xl" style={{color: "#0d4bf2"}}></i></button>
						</NavLink>
						<NavLink to="/logout">
							<button className={style.btn}>Logout</button>
						</NavLink>
					</div>
					}
		        </Navbar.Collapse>
		    </Container>
		</Navbar>
	)
}



