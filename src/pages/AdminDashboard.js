import {useContext, useEffect, useState} from "react";
import { Button, Table, Modal, Form } from "react-bootstrap";
import {Navigate, NavLink, Link} from "react-router-dom";
import Swal from "sweetalert2";
import style from './modules/AdminDashboard.module.css';
import UserContext from "../UserContext";
import "../components/modules/Navbar.css";

export default function AdminDashboard(){

	const { user, setUser } = useContext(UserContext);
	const [isMobile, setIsMobile] = useState(false);
	const [userId, setUserId] = useState(localStorage.getItem("userId"));
	const [userRole, setUserRole] = useState(localStorage.getItem("userRole"));
	
	// Create allProducts state to contain the products from the response of our fetch data.
	const [allProducts, setAllProducts] = useState([]);

	// State hooks to store the values of the input fields for our modal.
	const [productId, setProductId] = useState("");
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
    const [stocks, setStocks] = useState(0);
    const [image, setImage] = useState("");

    // State to determine whether submit button in the modal is enabled or not
    const [isActive, setIsActive] = useState(false);

    // State for Add/Edit Modal
    const [showAdd, setShowAdd] = useState(false);
	const [showEdit, setShowEdit] = useState(false);

	// To control the add product modal pop out
	const openAdd = () => setShowAdd(true); //Will show the modal
	const closeAdd = () => setShowAdd(false); //Will hide the modal


	const closeAddProduct = () => {

		// Clear input fields upon closing the modal
	    setName('');
	    setDescription('');
	    setPrice(0);
	    setStocks(0);
	    setImage('');

		setShowAdd(false);
	};

	// To control the edit product modal pop out
	// We have passed a parameter from the edit button so we can retrieve a specific product and bind it with our input fields.
	const openEdit = (id) => {
		setProductId(id);

		// Getting a specific product to pass on the edit modal
		fetch(`${process.env.REACT_APP_API_URL}/products/${id}`)
		.then(res => res.json())
		.then(data => {

			console.log(data);

			// updating the product states for editing
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
			setStocks(data.stocks);
			setImage(data.image);
		});

		setShowEdit(true)
	};

	const closeEdit = () => {

		// Clear input fields upon closing the modal
	    setName('');
	    setDescription('');
	    setPrice(0);
	    setStocks(0);
	    setImage('');

		setShowEdit(false);
	};


	// [SECTION] To view all products in the database (active & inactive)
	// fetchData() function to get all the active/inactive products.
	const fetchData = () =>{
		// get all the products from the database
		fetch(`${process.env.REACT_APP_API_URL}/products/all`, {
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setAllProducts(data.map(product => {
				return (
					<tr key={product._id} className={style.tr}>
						<td className={style.td}>{product._id}</td>
						<td className={style.td}><img className={style.img} src={product.image}/></td>
						<td className={style.td}>{product.name}</td>
						<td className={style.td}>₱{product.price}</td>
						<td className={style.td}>{product.stocks}</td>
						<td className={style.td}>{product.isActive ? "Active" : "Inactive"}</td>
						<td className={style.td}>
							{
							(product.isActive)
							?
								<>
								<button className={style.button1} onClick={() => openEdit(product._id)}><i class="fa-solid fa-pen-to-square"></i></button>
								<button className={style.button2} onClick={() => archive(product._id, product.name)}><i class="fa-solid fa-box-archive"></i></button>
								<button className={style.button3} onClick={() => deleteProduct(product._id)}><i class="fa-solid fa-trash"></i></button>
								</>
							:
								<>
								<button className={style.button1} onClick={() => openEdit(product._id)}><i class="fa-solid fa-pen-to-square"></i></button>
								<button className={style.button2} onClick={() => unarchive(product._id, product.name)}><i class="fa-solid fa-box-archive"></i></button>
								<button className={style.button3} onClick={() => deleteProduct(product._id)}><i class="fa-solid fa-trash"></i></button>
								</>
							}
						</td>
					</tr>	
		
				)
			}));
		});
	}

	// to fetch all product in the first render of the page.
	useEffect(()=>{
		fetchData();
	}, [])


	// [SECTION] Delete Product
	const deleteProduct = (id) =>{


		// Using the fetch method to set the isActive property of the product document to false
		fetch(`${process.env.REACT_APP_API_URL}/products/delete/${id}`, {
			method: "DELETE",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
			
		})
		.then(res => res.text())
		.then(data => {
			console.log(data)

			if(data){
				Swal.fire({
					title: "Delete Successful",
					icon: "success",
					text: `Product is now deleted.`
				});
				// To show the update with the specific operation intiated.
				fetchData();
			}
			else{
				Swal.fire({
					title: "Delete unsuccessful",
					icon: "error",
					text: "Something went wrong. Please try again later!"
				});
			}
		})
	}


	// [SECTION] Setting the product to Active/Inactive
	// Making the product inactive
	const archive = (id, productName) =>{
		console.log(id);
		console.log(productName);

		// Using the fetch method to set the isActive property of the product document to false
		fetch(`${process.env.REACT_APP_API_URL}/products/archive/${id}`, {
			method: "PATCH",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				isActive: false
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data){
				Swal.fire({
					title: "Archive Successful",
					icon: "success",
					text: `${productName} is now inactive.`
				});
				// To show the update with the specific operation intiated.
				fetchData();
			}
			else{
				Swal.fire({
					title: "Archive unsuccessful",
					icon: "error",
					text: "Something went wrong. Please try again later!"
				});
			}
		})
	}

	// Making the product active
	const unarchive = (id, productName) =>{
		console.log(id);
		console.log(productName);

		// Using the fetch method to set the isActive property of the product document to false
		fetch(`${process.env.REACT_APP_API_URL}/products/archive/${id}`, {
			method: "PATCH",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				isActive: true
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data){
				Swal.fire({
					title: "Unarchive Successful",
					icon: "success",
					text: `${productName} is now active.`
				});
				// To show the update with the specific operation intiated.
				fetchData();
			}
			else{
				Swal.fire({
					title: "Unarchive Unsuccessful",
					icon: "error",
					text: "Something went wrong. Please try again later!"
				});
			}
		})
	}

	// [SECTION] Adding a new product
	// Inserting a new product in our database
	const addProduct = (e) =>{
			// Prevents page redirection via form submission
		    e.preventDefault();

		    fetch(`${process.env.REACT_APP_API_URL}/products/add`, {
		    	method: "POST",
		    	headers: {
					"Content-Type": "application/json",
					"Authorization": `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
				    name: name,
				    description: description,
				    price: price,
				    stocks: stocks,
				    image: image
				})
		    })
		    .then(res => res.text())
		    .then(data => {
		    	console.log(data);

		    	if(data){
		    		Swal.fire({
		    		    title: "Product succesfully Added",
		    		    icon: "success",
		    		    text: `${name} is now added`
		    		});

		    		// To automatically add the update in the page
		    		fetchData();
		    		// Automatically closed the modal
		    		closeAddProduct();
		    	}
		    	else{
		    		Swal.fire({
		    		    title: "Error!",
		    		    icon: "error",
		    		    text: `Something went wrong. Please try again later!`
		    		});
		    		closeAddProduct();
		    	}

		    })

		    // Clear input fields
		    setName('');
		    setDescription('');
		    setPrice(0);
		    setStocks(0);
		    setImage('');
	}

	

	// [SECTION] Edit a specific product
	// Updating a specific product in our database
	// edit a specific product
	const editProduct = (e) =>{
			// Prevents page redirection via form submission
		    e.preventDefault();

		    fetch(`${process.env.REACT_APP_API_URL}/products/update/${productId}`, {
		    	method: "PATCH",
		    	headers: {
					"Content-Type": "application/json",
					"Authorization": `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
				    name: name,
				    description: description,
				    price: price,
				    stocks: stocks,
				    image: image
				})
		    })
		    .then(res => res.json())
		    .then(data => {
		    	console.log(data);

		    	if(data){
		    		Swal.fire({
		    		    title: "Product succesfully Updated",
		    		    icon: "success",
		    		    text: `${name} is now updated`
		    		});

		    		// To automatically add the update in the page
		    		fetchData();
		    		// Automatically closed the form
		    		closeEdit();

		    	}
		    	else{
		    		Swal.fire({
		    		    title: "Error!",
		    		    icon: "error",
		    		    text: `Something went wrong. Please try again later!`
		    		});

		    		closeEdit();
		    	}

		    })

		    // Clear input fields
		    setName('');
		    setDescription('');
		    setPrice(0);
		    setStocks(0);
		    setImage('');
	} 

	// Submit button validation for add/edit product
	useEffect(() => {

        // Validation to enable submit button when all fields are populated and set a price and stocks greater than zero.
        if(name !== "" && description !== "" && price > 0 && stocks > 0 && image !== ""){
            setIsActive(true);
        } else {
            setIsActive(false);
        }

    }, [name, description, price, stocks, image]);


	return(
		(userRole)
		?
		<>	
			<div className={style.navBG}>
				<nav className="navbar">
				  <Link to="/" className="home">
				  <h3 className="logo" style={{color:"fff"}}>Logo</h3>
				  </Link>
				  <ul className={isMobile? "nav-links-mobile" : "nav-links"}
				  onClick={() => setIsMobile(false)}
				  >
				   
				    <Link to="/logout" className="signup">
				      <li style={{color:"black"}}>Sign Out</li>
				    </Link>

				  </ul>
				  <button 
				  className="mobile-menu-icon"
				  onClick={() => setIsMobile(!isMobile)}
				  >
				    {isMobile ? 
				    <i className="fas fa-times"></i> 
				    : 
				    <i className="fas fa-bars"></i>}
				  </button>
				</nav>
			</div>

			<div className={style.Icon}>
				<h1>Admin Dashboard</h1>

				<NavLink to="/admin">
				<button className={style.button4}><i class="fa-solid fa-house"></i></button>
				</NavLink>
				
				<NavLink to="/userDashboard">
				<button className={style.button4}><i class="fa-solid fa-users"></i></button>
				</NavLink>

				<NavLink to="/orderDashboard">
				<button className={style.button5}><i class="fa-solid fa-truck"></i></button>
				</NavLink>
				
			</div>
			{/*End of admin dashboard header*/}

			{/*For view all the product in the database.*/}
			<div className={style.tableContainer}>
				<div className={style.tableHeader}>
					<p className={style.text}>Product Details</p>
					<div>
						<input className={style.input} placeholder="product" />
						<button className={style.addNew} onClick={openAdd}>+ Add New</button>
					</div>
				</div>
				<div className={style.tableSection}>
					<table className={style.table}>
						<thead>
							<tr>
								<th className={style.th}>Id No.</th>
								<th className={style.th}>Product</th>
								<th className={style.th}>Name</th>
								<th className={style.th}>Price</th>
								<th className={style.th}>Stocks</th>
								<th className={style.th}>Status</th>
								<th className={style.th}>Action</th>
							</tr>	
						</thead>
						<tbody>
							{allProducts}
						</tbody>
					</table>
				</div>
			</div>
			{/*End of table for product viewing*/}

	    	{/*Modal for Adding a new product*/}
	        <Modal show={showAdd}  onHide={closeAdd} className="pt-5">
	    		<Form onSubmit={e => addProduct(e)}>

	    			<Modal.Header closeButton>
	    				<Modal.Title>Add New Product</Modal.Title>
	    			</Modal.Header>

	    			<Modal.Body>
	    	        	<Form.Group controlId="name" className="mb-3">
	    	                <Form.Label>Product Name</Form.Label>
	    	                <Form.Control 
	    		                type="text" 
	    		                placeholder="Enter Product Name" 
	    		                value = {name}
	    		                onChange={e => setName(e.target.value)}
	    		                required
	    	                />
	    	            </Form.Group>

	    	            <Form.Group controlId="description" className="mb-3">
	    	                <Form.Label>Product Description</Form.Label>
	    	                <Form.Control
	    	                	as="textarea"
	    	                	rows={3}
	    		                placeholder="Enter Product Description" 
	    		                value = {description}
	    		                onChange={e => setDescription(e.target.value)}
	    		                required
	    	                />
	    	            </Form.Group>

	    	            <Form.Group controlId="price" className="mb-3">
	    	                <Form.Label>Product Price</Form.Label>
	    	                <Form.Control 
	    		                type="number" 
	    		                placeholder="Enter Product Price" 
	    		                value = {price}
	    		                onChange={e => setPrice(e.target.value)}
	    		                required
	    	                />
	    	            </Form.Group>

	    	            <Form.Group controlId="stocks" className="mb-3">
	    	                <Form.Label>Product Stockss</Form.Label>
	    	                <Form.Control 
	    		                type="number" 
	    		                placeholder="Enter Product Stocks" 
	    		                value = {stocks}
	    		                onChange={e => setStocks(e.target.value)}
	    		                required
	    	                />
	    	            </Form.Group>

	    	            <Form.Group controlId="image" className="mb-3">
	    	                <Form.Label>Product Image</Form.Label>
	    	                <Form.Control 
	    		                type="textarea"
	    		                rows={2} 
	    		                placeholder="Add Image URL" 
	    		                value = {image}
	    		                onChange={e => setImage(e.target.value)}
	    		                required
	    	                />
	    	            </Form.Group>

	    			</Modal.Body>

	    			<Modal.Footer>
	    				{ isActive 
	    					? 
	    					<Button variant="primary" type="submit" id="submitBtn">
	    						Save
	    					</Button>
	    				    : 
	    				    <Button variant="danger" type="submit" id="submitBtn" disabled>
	    				    	Save
	    				    </Button>
	    				}
	    				<Button variant="secondary" onClick={closeAdd}>
	    					Close
	    				</Button>
	    			</Modal.Footer>

	    		</Form>	
	    	</Modal>
	    {/*End of modal for adding product*/}

    	{/*Modal for Editing a product*/}
        <Modal show={showEdit} onHide={closeEdit} className="pt-5">
    		<Form onSubmit={e => editProduct(e)}>

    			<Modal.Header closeButton>
    				<Modal.Title>Edit a Product</Modal.Title>
    			</Modal.Header>

    			<Modal.Body>
    	        	<Form.Group controlId="name" className="mb-3">
    	                <Form.Label>Product Name</Form.Label>
    	                <Form.Control 
    		                type="text" 
    		                placeholder="Enter Product Name" 
    		                value = {name}
    		                onChange={e => setName(e.target.value)}
    		                required
    	                />
    	            </Form.Group>

    	            <Form.Group controlId="description" className="mb-3">
    	                <Form.Label>Product Description</Form.Label>
    	                <Form.Control
    	                	as="textarea"
    	                	rows={3}
    		                placeholder="Enter Product Description" 
    		                value = {description}
    		                onChange={e => setDescription(e.target.value)}
    		                required
    	                />
    	            </Form.Group>

    	            <Form.Group controlId="price" className="mb-3">
    	                <Form.Label>Product Price</Form.Label>
    	                <Form.Control 
    		                type="number" 
    		                placeholder="Enter Product Price" 
    		                value = {price}
    		                onChange={e => setPrice(e.target.value)}
    		                required
    	                />
    	            </Form.Group>

    	            <Form.Group controlId="stocks" className="mb-3">
    	                <Form.Label>Product Stocks</Form.Label>
    	                <Form.Control 
    		                type="number" 
    		                placeholder="Enter Product Stocks" 
    		                value = {stocks}
    		                onChange={e => setStocks(e.target.value)}
    		                required
    	                />
    	            </Form.Group>

                    <Form.Group controlId="image" className="mb-3">
                        <Form.Label>Product Image</Form.Label>
                        <Form.Control 
        	                type="textarea"
        	                rows={2} 
        	                placeholder="Add Image URL" 
        	                value = {image}
        	                onChange={e => setImage(e.target.value)}
        	                required
                        />
                    </Form.Group>

    			</Modal.Body>

    			<Modal.Footer>
    				{ isActive 
    					? 
    					<Button variant="primary" type="submit" id="submitBtn">
    						Save
    					</Button>
    				    : 
    				    <Button variant="danger" type="submit" id="submitBtn" disabled>
    				    	Save
    				    </Button>
    				}
    				<Button variant="secondary" onClick={closeEdit}>
    					Close
    				</Button>
    			</Modal.Footer>

    		</Form>	
    	</Modal>
    	{/*End of modal for editing a product*/}
		</>
		:
		<Navigate to="/login" />
	)
}