import { useEffect, useState, useContext } from "react";
import { Navigate, Link } from "react-router-dom";
import style from './modules/Product.module.css';
import HomeProductCard from "../components/HomeProductCard";
import UserContext from "../UserContext";
import Footer from '../components/Footer' ;
import { Fragment } from 'react';




export default function HomeProducts() {

  const { user, setUser } = useContext(UserContext);

  const [products, setProducts] = useState([]);

  useEffect(() =>{
    // Will retrieve all the active products
    fetch(`${process.env.REACT_APP_API_URL}/products/active`)
    .then(res => res.json())
    .then(data => {
      console.log(data);
      setProducts(data.map(product =>{
        return(
          
          <HomeProductCard key={product._id} productProp={product}/>
        );
      }));
    })
  }, []);



  return(
    (user.isAdmin)
    ?
      <Navigate to="/admin" />
    : 
    <>  
    <Fragment>
      <div className={style.whole}>
        <div className={style.container}>
          <h2 className="text-center pt-5">Explore Our Fruits</h2>
          <div class={style.wrapper}>
            {products}
          </div>
        </div>
      </div>
      <Footer/>
    </Fragment> 
    </>
  )
}