import { useState, useEffect, useContext } from "react";
import style from './modules/ProductView.module.css';
import {Container, Card, Button, Row, Col} from "react-bootstrap";
import { useParams, useNavigate, NavLink, Link } from "react-router-dom";
import Swal from "sweetalert2";
import star from './images/star.png'
import Footer from '../components/Footer' ;
import { Fragment } from 'react';

import UserContext from "../UserContext";

export default function ProductView(){

	const { user, setUser } = useContext(UserContext);
	const [userId, setUserId] = useState(localStorage.getItem("userId"));
	const { productId } = useParams();
	const navigate = useNavigate();
	const [quantity, setQuantity] = useState(0);

	const [isMobile, setIsMobile] = useState(false);

	const Increment = () =>{
		setQuantity(quantity + 1)
	}
	const Decrement = () =>{
		let value = 0
		if(quantity <= value){ 
			value = 1
		}
		else{
			value = quantity
		}

		setQuantity(value-1)
	}

	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState();
	const [stocks, setStocks] = useState();
	const [image, setImage] = useState('');

	useEffect(()=>{


		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {


			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
			setStocks(data.stocks);
			setImage(data.image);

		});

	}, [productId])

	

	const checkOut = () =>{

		fetch(`${process.env.REACT_APP_API_URL}/orders/${productId}/makeOrder`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			},
			body: JSON.stringify({
				quantity: quantity
			})
		})
		.then(res => res.text())
		.then(data => {


			if(data){
				Swal.fire({
					title: "Successfully Check Out",
					icon: "success",
					text: "You have successfully check out this products."
				});

				navigate("/products");
			}
			else{
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				});
			}

		});

	}

	return(
		<Fragment>
			<div className={style.Wrapper}>

				<nav className="navbar">
				  <Link to="/" className="home">
				  <h3 className="logo">Logo</h3>
				  </Link>
				  <ul className={isMobile? "nav-links-mobile" : "nav-links"}
				  onClick={() => setIsMobile(false)}
				  >
				    <Link to="/" className="home">
				      <li>Home</li>
				    </Link>

				    <Link to="/contacts" className="products">
				      <li>Contact</li>
				    </Link>

				    <Link to="/products" className="products">
				      <li>Products</li>
				    </Link>

				    {(user.id === null && userId === null)?
				    <>
				    <Link to="/register" className="products">
				      <li>Sign Up</li>
				    </Link>

				    <Link to="/login" className="signup">
				      <li>Sign In</li>
				    </Link>
				    </>
				    :
				    <>
				    <Link to="/order" className="products">
				      <li>Orders</li>
				    </Link>

				    <Link to="/logout" className="signup">
				      <li>Sign Out</li>
				    </Link>
				    </>
				    }

				    
				  </ul>
				  <button 
				  className="mobile-menu-icon"
				  onClick={() => setIsMobile(!isMobile)}
				  >
				    {isMobile ? 
				    <i className="fas fa-times"></i> 
				    : 
				    <i className="fas fa-bars"></i>}
				  </button>
				</nav>

				<div className={style.container}>
			        <div className={style.ImgContainer}>
			          <img className={style.Image} src={image} />
			        </div>
			        <div className={style.InfoContainer}>
			          <div className={style.Title}>{name}</div>
			          <div className={style.Desc}>{description}</div>

			          	<div className={style.ratings}>
	 						<img src={star} className={style.star}/>
							<img src={star} className={style.star}/>
	 						<img src={star} className={style.star}/>
	 						<img src={star} className={style.star}/>
	 						<img src={star} className={style.star}/>
	 						<span className={style.ratingCount}>4,023 reviews</span>
	 					</div>
	 					
			          <div className={style.Price}>₱ {price}</div>
			          <p>Stocks : {stocks}</p>
			          
			          <div className={style.AddContainer}>
			            <div className={style.AmountContainer}>
			            	<button className={style.Add} onClick={() => Decrement()}>-</button>
			           		<div className={style.Quantity}>{quantity}</div>
			           		<button className={style.Add} onClick={() => Increment()}>+</button>
			            </div>
			            {
						(user.id === null && userId === null)
						?
						<NavLink to="/login">
							<button className={style.Button}>Login to Check Out</button>
						</NavLink>
						:
			            <button className={style.Button} onClick={() => checkOut()}>CHECK OUT</button>
			        	}
			          </div>
			        </div>
		        </div>
	      	</div>
		
		<Footer/>
	</Fragment>		
	)
}