import style from './modules/Error.module.css';


export default function Error(){

	return(
		<div className={style.container}>
			<img className={style.Img} src="https://www.reshot.com/preview-assets/illustrations/CL6RV2ZXTW/404-error-landing-page-CL6RV2ZXTW-w1600.jpg"/>
		</div>
	)
}