import { Fragment } from 'react';
import LandingPage from '../components/LandingPage' ;
import Categories from '../components/Categories' ;
import Featured from '../components/Featured' ;
import About from '../components/About' ;
import HomeProducts from './HomeProducts';
import Marquee from '../components/Marquee' ;



export default function Home() {

  return(
    <Fragment>
      <LandingPage/>
      <Featured/>
      <About/>
      <Categories/>
      <Marquee/>
      <HomeProducts/>
    </Fragment>
  )
}