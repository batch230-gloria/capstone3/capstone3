import {useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext';
import {Navigate, Link} from 'react-router-dom';
import Swal from 'sweetalert2'
import style from './modules/Login.module.css';
import "../components/modules/Navbar.css";



export default function Login() {

	const { user, setUser } = useContext(UserContext);
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(false);
	const [isMobile, setIsMobile] = useState(false);
  	const [userId, setUserId] = useState(localStorage.getItem("userId"));


	function authenticate(e){
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
			method: 'POST',
			headers: { 'Content-type' : 'application/json' },
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			// console.log(data);
			// console.log("Check accessToken");
			// console.log(data.accessToken);
			
			if(typeof data.accessToken !== "undefined"){
				localStorage.setItem('token', data.accessToken);
				retrieveUserDetails(data.accessToken);

				Swal.fire({
					title: "Login Successful",
					icon: "success",
					text: "Welcome to Fresh Fruits!"
				})
			}

			else{
				Swal.fire({
					title:"Authentication failed",
					icon: "success",
					text: "Check your login details and try again."
				})
			}
		})

		setEmail('');
	}

	const retrieveUserDetails = (token) => {
        fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {
            // console.log(data);

            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })


            localStorage.setItem("user", data._id);
            localStorage.setItem("userId", data._id);
            localStorage.setItem("userRole", data.isAdmin);

            // console.log("Login: ");
            // console.log(user)
            
        })
    }


	useEffect(() => {
		if(email !== '' && password !== ''){
			setIsActive(true);
		}
		else{
			setIsActive(false);
		}
	}, [email, password])



	return(
		(user.id !== null)
		? 
		<Navigate to ="/products"/>
		: 

		<div className={style.container}>

			<nav className="navbar">
			  <Link to="/" className="home">
			  <h3 className="logo">Logo</h3>
			  </Link>
			  <ul className={isMobile? "nav-links-mobile" : "nav-links"}
			  onClick={() => setIsMobile(false)}
			  >
			    <Link to="/" className="home">
			      <li>Home</li>
			    </Link>

			    <Link to="/contacts" className="products">
			      <li>Contact</li>
			    </Link>

			    <Link to="/products" className="products">
			      <li>Products</li>
			    </Link>

			    {(user.id === null && userId === null)?
			    <>
			    <Link to="/register" className="products">
			      <li>Sign Up</li>
			    </Link>

			    <Link to="/login" className="signup">
			      <li>Sign In</li>
			    </Link>
			    </>
			    :
			    <>
			    <Link to="/order" className="products">
			      <li>Orders</li>
			    </Link>

			    <Link to="/logout" className="signup">
			      <li>Sign Out</li>
			    </Link>
			    </>
			    }

			    
			  </ul>
			  <button 
			  className="mobile-menu-icon"
			  onClick={() => setIsMobile(!isMobile)}
			  >
			    {isMobile ? 
			    <i className="fas fa-times"></i> 
			    : 
			    <i className="fas fa-bars"></i>}
			  </button>
			</nav>
			
			<div className={style.formContainer}>
				<form className={style.form} onSubmit = {(event) => authenticate(event)} >
				    <p className={style.heading}><i class="fa-solid fa-right-to-bracket fa-lg" style={{color: "#007FFF"}}></i>  Sign In</p>
				    <span className={style.subtitle}>Get started with our app, just create an account and enjoy the experience.</span>
				    <input className={style.input}
				    placeholder="Email" 
				    type="email"
				    value={email}
				    onChange={(e) => setEmail(e.target.value)}
				    required

				    />
				    <input className={style.input}
				    placeholder="Password" 
				    type="password" 
				    value={password}
				    onChange={(e) => setPassword(e.target.value)}
				    required
				    />
				    {isActive
				    ?
				    <button className={style.btn} type="submit">Submit</button>
				    :
				    <button className={style.btn} type="submit" disabled>Submit</button>
				    }

				    <span className={style.subtitle2}>Don't have an account?
				    <Link to="/register" className={style.signupLink}>
				    	<p>Sign Up</p>
				    </Link>
				    </span>
				</form>


			</div>
		</div>

	)

}