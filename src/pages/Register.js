import React from 'react';
import register from "./modules/Register.module.css";
import { useState, useEffect, useContext } from 'react';
import {Navigate, useNavigate, Link} from 'react-router-dom';
import UserContext from '../UserContext'
import Swal from 'sweetalert2';
import "../components/modules/Navbar.css";

export default function Register (){

	const { user, setUser } = useContext(UserContext);
	const [isMobile, setIsMobile] = useState(false);
 	const [userId, setUserId] = useState(localStorage.getItem("userId"));

	const navigate = useNavigate();

	const [fName, setFName] = useState('');
	const [lName, setLName] = useState('');
	const [email, setEmail] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [password1, setPassword1] = useState('');
	
	const [isActive, setIsActive] = useState(false);

	
	// console.log(fName);
	// console.log(lName);
	// console.log(email);
	// console.log(mobileNo);
	// console.log(password1);


	useEffect(() =>{

	    if((fName !== '' && lName !=='' && email !== '' && mobileNo !== '' && password1 !== '')){
	        setIsActive(true);
	    }
	    else{
	        setIsActive(false);
	    }

	}, [fName, lName, email, mobileNo, password1])


	function registerUser(e){

	    e.preventDefault();

	    fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
	        method: "POST",
	        headers:{
	            "Content-Type": "application/json"
	        },
	        body: JSON.stringify({
	            email: email
	        })
	    })
	    .then(res => res.json())
	    .then(data =>{
	        // console.log(data); 

	        if(data === true){
	            Swal.fire({
	                title: "Duplicate email found",
	                icon: "error",
	                text: "Kindly provide another email to complete the registration."
	            })


	        }
	        else{

	            fetch(`${process.env.REACT_APP_API_URL}/users/register`,{
	                method: "POST",
	                headers:{
	                    "Content-Type": "application/json"
	                },
	                body: JSON.stringify({
	                    firstName: fName,
	                    lastName: lName,
	                    email: email,
	                    password: password1,
	                    mobileNo: mobileNo
	                })
	            })
	            .then(res => res.json())
	            .then(data => {
	                // console.log(data);

	                if(data){
	                    Swal.fire({
	                        title: "Registration Successful",
	                        icon: "success",
	                        text: "Welcome to Fresh Fruits!"
	                    });

	                    setFName('');
	                    setLName('');
	                    setEmail('');
	                    setMobileNo('');
	                    setPassword1('');


	                    navigate("/login");
	                }
	                else{

	                    Swal.fire({
	                        title: "Something went wrong",
	                        icon: "error",
	                        text: "Please try again."
	                    });

	                }
	            })


	        }
	    })
	}


	return(
		
		<div className={register.container}>

			<nav className="navbar">
			  <Link to="/" className="home">
			  <h3 className="logo">Logo</h3>
			  </Link>
			  <ul className={isMobile? "nav-links-mobile" : "nav-links"}
			  onClick={() => setIsMobile(false)}
			  >
			    <Link to="/" className="home">
			      <li>Home</li>
			    </Link>

			    <Link to="/contacts" className="products">
			      <li>Contact</li>
			    </Link>

			    <Link to="/products" className="products">
			      <li>Products</li>
			    </Link>

			    {(user.id === null && userId === null)?
			    <>
			    <Link to="/register" className="products">
			      <li>Sign Up</li>
			    </Link>

			    <Link to="/login" className="signup">
			      <li>Sign In</li>
			    </Link>
			    </>
			    :
			    <>
			    <Link to="/order" className="products">
			      <li>Orders</li>
			    </Link>

			    <Link to="/logout" className="signup">
			      <li>Sign Out</li>
			    </Link>
			    </>
			    }

			    
			  </ul>
			  <button 
			  className="mobile-menu-icon"
			  onClick={() => setIsMobile(!isMobile)}
			  >
			    {isMobile ? 
			    <i className="fas fa-times"></i> 
			    : 
			    <i className="fas fa-bars"></i>}
			  </button>
			</nav>
			
			<div className={register.formContainer}>	
				<form className={register.form} onSubmit={(event => registerUser(event))} >
				    <p className={register.heading}><i class="fa-solid fa-user-plus"></i>  Sign Up</p>
				    <span className={register.subtitle}>Get started with our app, just create an account and enjoy the experience.</span>


				    <input className={register.input}
				    placeholder="First Name" 
				    type="text"
				    value={fName}
				    onChange={(e) => setFName(e.target.value)}
				    required
				    />

				    <input className={register.input}
				    placeholder="Last Name" 
				    type="text"
				    value={lName}
				    onChange={(e) => setLName(e.target.value)}
				    required
				    />

				    <input className={register.input}
				    placeholder="Email" 
				    type="email"
				    value={email}
				    onChange={(e) => setEmail(e.target.value)}
				    required
				    />

				    <input className={register.input}
				    placeholder="Mobile Number" 
				    type="string"
				    value={mobileNo}
				    onChange={(e) => setMobileNo(e.target.value)}
				    required
				    />

				    <input className={register.input}
				    placeholder="Password" 
				    type="password" 
				    value={password1}
				    onChange={(e) => setPassword1(e.target.value)}
				    required
				    />

				    {isActive
				    ?
				    <button className={register.btn} type="submit">Submit</button>
				    :
				    <button className={register.btn} type="submit" disabled>Submit</button>
				    }

				    <span className={register.subtitle2}>Already have an account?
				    <Link to="/login" className={register.signupLink}>
				    	<p>Sign In</p>
				    </Link>
				    </span>
				</form>
			</div>
		</div>
		
	)
};