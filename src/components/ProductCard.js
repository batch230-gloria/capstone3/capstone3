
import { useState, useEffect } from "react";
import { Card, Button } from 'react-bootstrap';
import {Link, NavLink} from 'react-router-dom';
import style from './modules/ProductCard.module.css';


export default function ProductCard({productProp}) {

  const { _id, name, description, price, stocks, image } = productProp;

  return (

      <div className={style.product}>
        <img className={style.img} src={image}/>
        <div className={style.productInfo}>
          <div className={style.title}>{name}</div>
          <div className={style.amount}>₱{price}</div>
        </div>
        
        <NavLink to={`/products/${_id}`}>
          <button className={style.order}>ORDER NOW</button>
          </NavLink>
        
      </div>
  )
}


