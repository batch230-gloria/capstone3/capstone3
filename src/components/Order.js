import {useContext, useEffect, useState} from "react";
import {Navigate, NavLink, Link} from "react-router-dom";
import style from './modules/Order.module.css';
import UserContext from "../UserContext";
import Footer from './Footer' ;
import { Fragment } from 'react';
import "./modules/Navbar.css";


export default function Order(){

	const [isMobile, setIsMobile] = useState(false);
	const [userId, setUserId] = useState(localStorage.getItem("userId"));

	const { user, setUser } = useContext(UserContext);

	const [allUsers, setAllUsers] = useState([]);

	const fetchData = () =>{
		fetch(`${process.env.REACT_APP_API_URL}/users/orders`, {
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setAllUsers(data.map(user => {
				return (
				   
					<div className={style.box}>
						<img className={style.img} src={user.image}/>
						<div className={style.content}>
							<h3 className={style.h3}>{user.name}</h3>
							<p>Product Id: </p>
							<p className={style.productId}>{user.productId}</p>
							<p>Quantity: {user.quantity}</p>
						</div>
						<div className={style.right}>
							<p>email: {user.email}</p>
							<h5 className={style.h5}>Total Amount: ₱ {user.totalAmount}</h5>
						</div>
					</div>
				)
			}));
		});
	}

	useEffect(()=>{
		fetchData();
	}, [])

	return(

		<Fragment>
		
				<div className={style.container}>

					<nav className="navbar">
					  <Link to="/" className="home">
					  <h3 className="logo">Logo</h3>
					  </Link>
					  <ul className={isMobile? "nav-links-mobile" : "nav-links"}
					  onClick={() => setIsMobile(false)}
					  >
					    <Link to="/" className="home">
					      <li>Home</li>
					    </Link>

					    <Link to="/contacts" className="products">
					      <li>Contact</li>
					    </Link>

					    <Link to="/products" className="products">
					      <li>Products</li>
					    </Link>

					    {(user.id === null && userId === null)?
					    <>
					    <Link to="/register" className="products">
					      <li>Sign Up</li>
					    </Link>

					    <Link to="/login" className="signup">
					      <li>Sign In</li>
					    </Link>
					    </>
					    :
					    <>
					    <Link to="/order" className="products">
					      <li>Orders</li>
					    </Link>

					    <Link to="/logout" className="signup">
					      <li>Sign Out</li>
					    </Link>
					    </>
					    }

					    
					  </ul>
					  <button 
					  className="mobile-menu-icon"
					  onClick={() => setIsMobile(!isMobile)}
					  >
					    {isMobile ? 
					    <i className="fas fa-times"></i> 
					    : 
					    <i className="fas fa-bars"></i>}
					  </button>
					</nav>

					<div className={style.wrapper}>
						<h1 className={style.h1}>My Orders</h1>
						<div className={style.project}>
							<div className={style.shop}>

							{allUsers}

							</div>
						</div>
					</div>
				</div>
			
		<Footer/>		
		</Fragment>

	)

}