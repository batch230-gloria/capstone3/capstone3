import {useContext, useEffect, useState} from "react";

import { Button, Table, Modal, Form } from "react-bootstrap";
import {Navigate, NavLink} from "react-router-dom";
import Swal from "sweetalert2";
import style from './modules/UserDashboard.module.css';
import UserContext from "../UserContext";

export default function UserDashboard(){

	// const { user } = useContext(UserContext);
	const [userRole, setUserRole] = useState(localStorage.getItem("userRole"));
	
	// Create allusers state to contain the users from the response of our fetch data.
	const [allOrders, setAllOrders] = useState([]);


	const fetchData = () =>{
		fetch(`${process.env.REACT_APP_API_URL}/users/allOrders`, {
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {

			setAllOrders(data.map(order => {
				return (
					<tr key={order._id} className={style.tr}>
						<td className={style.td}>{order._id}</td>
						<td className={style.td}><img className={style.img} src={order.image}/></td>
						<td className={style.td}>{order.userId}</td>
						<td className={style.td}>{order.email}</td>
						<td className={style.td}>{order.productId}</td>
						<td className={style.td}>{order.name}</td>
						<td className={style.td}>{order.quantity}</td>
						<td className={style.td}>{order.totalAmount}</td>

						<td className={style.td}>
						<button className={style.button3} onClick={() => deleteOrder(order._id)}><i class="fa-solid fa-trash"></i></button>
						</td>
						
					</tr>	
		
				)
			}));
		});
	}

	// to fetch all user in the first render of the page.
	useEffect(()=>{
		fetchData();
	}, [])

	// [SECTION] Delete Order
	const deleteOrder = (id) =>{
		console.log(id);

		fetch(`http://localhost:3005/orders/delete/${id }`, {
			method: "DELETE",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
			
		})
		.then(res => res.json())
		.then(data => {

			if(data){
				Swal.fire({
					title: "Delete Successful",
					icon: "success",
					text: `Order is now deleted.`
				});

				fetchData();
			}
			else{
				Swal.fire({
					title: "Delete unsuccessful",
					icon: "error",
					text: "Something went wrong. Please try again later!"
				});
			}
		})
	}



		return(
			(userRole)
			?
			<>
				<div className={style.Icon}>
					<h1>Admin Dashboard</h1>

					<NavLink to="/admin">
					<button className={style.button4}><i class="fa-solid fa-house"></i></button>
					</NavLink>
					
					<NavLink to="/userDashboard">
					<button className={style.button4}><i class="fa-solid fa-users"></i></button>
					</NavLink>

					<NavLink to="/orderDashboard">
					<button className={style.button5}><i class="fa-solid fa-truck"></i></button>
					</NavLink>
					
				</div>


				{/*For view all the user in the database.*/}
				<div className={style.tableContainer}>
					<div className={style.tableHeader}>
						<p className={style.text}>Order Details</p>
						<div>
							<input className={style.input} placeholder="order" />
							{/*<button className={style.addNew} onClick={openAdd}>+ Add New</button>*/}
						</div>
					</div>
					<div className={style.tableSection}>
						<table className={style.table}>
							<thead>
								<tr>
									<th className={style.th}>Id No.</th>
									<th className={style.th}>Product</th>
									<th className={style.th}>User Id</th>
									<th className={style.th}>Email</th>
									<th className={style.th}>Product Id</th>
									<th className={style.th}>Name</th>
									<th className={style.th}>Quantity</th>
									<th className={style.th}>Total Amount</th>
									<th className={style.th}>Action</th>

								</tr>	
							</thead>
							<tbody>
								{allOrders}
							</tbody>
						</table>
					</div>
				</div>

			</>
			:
			<Navigate to="/login" />
		)



}