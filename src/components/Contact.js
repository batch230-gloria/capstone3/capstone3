import React from 'react';
import {Link} from "react-router-dom";
import "./modules/Contact.css";
import contact from './images/contact.png';
import UserContext from "../UserContext";
import {useState, Fragment, useContext} from 'react';
import "../components/modules/Navbar.css";

export default function Contact(){

	const [isMobile, setIsMobile] = useState(false);
	const [userId, setUserId] = useState(localStorage.getItem("userId"));

	const { user, setUser } = useContext(UserContext);

	return(
		<section class = "contact-section">

			<div class = "contact-bg">

				<nav className="navbar">
				  <Link to="/" className="home">
				  <h3 className="logo">Logo</h3>
				  </Link>
				  <ul className={isMobile? "nav-links-mobile" : "nav-links"}
				  onClick={() => setIsMobile(false)}
				  >
				    <Link to="/" className="home">
				      <li>Home</li>
				    </Link>

				    <Link to="/contacts" className="products">
				      <li>Contact</li>
				    </Link>

				    <Link to="/products" className="products">
				      <li>Products</li>
				    </Link>

				    {(user.id === null && userId === null)?
				    <>
				    <Link to="/register" className="products">
				      <li>Sign Up</li>
				    </Link>

				    <Link to="/login" className="signup">
				      <li>Sign In</li>
				    </Link>
				    </>
				    :
				    <>
				    <Link to="/order" className="products">
				      <li>Orders</li>
				    </Link>

				    <Link to="/logout" className="signup">
				      <li>Sign Out</li>
				    </Link>
				    </>
				    }

				    
				  </ul>
				  <button 
				  className="mobile-menu-icon"
				  onClick={() => setIsMobile(!isMobile)}
				  >
				    {isMobile ? 
				    <i className="fas fa-times"></i> 
				    : 
				    <i className="fas fa-bars"></i>}
				  </button>
				</nav>

				<div class = "infoContainer">
					<h3>Have a question or need more info?</h3>
					<h2>contact us</h2>
					<div class = "line">
					  <div></div>
					  <div></div>
					  <div></div>
					</div>
					<p class = "text">If you have a question about the Fresh-fruits platform, or would like to know more about our roadmap for fresh fruit and vegetable quality control, please leave your details or reach out directly at info@fresh-fruit.com, and we’ll get back to you as soon as possible.</p>
				</div>	
			</div>


			<div class = "contact-body">
			<div class = "contact-info">
			  <div>
			    <span><i class = "fas fa-mobile-alt"></i></span>
			    <span>Phone No.</span>
			    <span class = "text">1-2345-67891-0</span>
			  </div>
			  <div>
			    <span><i class = "fas fa-envelope-open"></i></span>
			    <span>E-mail</span>
			    <span class = "text">mail@company.com</span>
			  </div>
			  <div>
			    <span><i class = "fas fa-map-marker-alt"></i></span>
			    <span>Address</span>
			    <span class = "text">2nd Level Market! Market! Mabini Ave. cor. Mckinley Parkway Fort Bonifacio Taguig</span>
			  </div>
			  <div>
			    <span><i class = "fas fa-clock"></i></span>
			    <span>Opening Hours</span>
			    <span class = "text">Monday - Friday (9:00 AM to 5:00 PM)</span>
			  </div>
			</div>

			<div class = "contact-form">
			  <form>
			    <div>
			      <input type = "text" class = "form-control" placeholder="First Name"/>
			      <input type = "text" class = "form-control" placeholder="Last Name"/>
			    </div>
			    <div>
			      <input type = "email" class = "form-control" placeholder="E-mail"/>
			      <input type = "text" class = "form-control" placeholder="Phone"/>
			    </div>
			    <textarea rows = "5" placeholder="Message" class = "form-control"></textarea>
			    <input type = "submit" class = "send-btn" value = "send message"/>
			  </form>

			  <div>
			    <img src ={contact}/>
			  </div>
			</div>
			</div>

			<div class = "map">
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15447.389853912104!2d121.04849781877198!3d14.55071403705659!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3397c8f2ca652c83%3A0x62c8011c6abe662e!2sMarket*21%20Market*21!5e0!3m2!1sen!2sph!4v1680438834375!5m2!1sen!2sph" width="100%" height="450" style={{border:'0'}} allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
			</div>

			<div class = "contact-footer">
			<h3>Follow Us</h3>
			<div class = "social-links">
			  <a href = "#" class = "fab fa-facebook-f"></a>
			  <a href = "#" class = "fab fa-twitter"></a>
			  <a href = "#" class = "fab fa-instagram"></a>
			  <a href = "#" class = "fab fa-linkedin"></a>
			  <a href = "#" class = "fab fa-youtube"></a>
			</div>
			</div>
		</section>
	)
}