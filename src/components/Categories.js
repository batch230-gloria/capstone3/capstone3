import Category from './Category';
import { categories } from "../data";
import style from './modules/Categories.module.css';

export default function Categories(){

	return(
		<div className={style.Container}>
			{categories.map((item)=>(
				<Category item={item} key={item.id}/>
			))}
		</div>
	)
}