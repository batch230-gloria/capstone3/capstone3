import {Link, NavLink, Navigate} from 'react-router-dom';
import style from './modules/Category.module.css';

export default function Category ({item}) {


  const imageClick = () => {
    <Navigate to ="/login"/>
  }

  return (
    <div className={style.Container}>
      <Link to="/products">
      <img className={style.Image} src={item.img} />
      <div className={style.Info}>
        <h5 className={style.text}>SHOP NOW</h5>
        <h1 className={style.Title}>{item.title}</h1>
      </div>
      </Link>
    </div>
  )
}

