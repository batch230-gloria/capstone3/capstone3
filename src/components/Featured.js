import {Navigate, NavLink} from "react-router-dom";
import style from './modules/Featured.module.css';
import apple from './images/apple.jpg';
import pineapple from './images/pineapple.jpg';
import avocado from './images/avocado.jpg';
import banana from './images/banana.jpg';
import orange from './images/orange.jpg';
import cherry from './images/cherry.jpg';
import kiwi from './images/kiwi.jpg';

export default function Featured(){
	
	return(
		<div className={style.background}>
		<h1 className={style.page}>FEATURED FRUITS</h1>
		<div className={style.container}>
			<div className={style.card}>
				<img src={cherry} className={style.cardImg}/>
				<div className={style.Body}>
					<h1 className={style.Title}>CHERRY</h1>
					<p className={style.sub}>₱ 10</p>
					<p className={style.info}>Cherry help reduce inflammation and exercise-induced muscle soreness, lower blood pressure and improve sleep.</p>
					<NavLink to={`/products/641f879dbcce204df8140bbb`}>
					<button className={style.btn}>BUY NOW</button>
					</NavLink>
				</div>
			</div>

			<div className={style.card}>
				<img src={avocado} className={style.cardImg}/>
				<div className={style.Body}>
					<h1 className={style.Title}>AVOCADO</h1>
					<p className={style.sub}>₱ 200</p>
					<p className={style.info}>Avocados are have rich vitamins, minerals, and healthy fats to help prevent disease.</p>
					<NavLink to={`/products/641f84a1bcce204df8140b92`}>
					<button className={style.btn}>BUY NOW</button>
					</NavLink>
				</div>
			</div>

			{/*<div className={style.card}>
				<img src={apple} className={style.cardImg}/>
				<div className={style.Body}>
					<h1 className={style.Title}>APPLE</h1>
					<p className={style.sub}>₱ 20</p>
					<p className={style.info}>Apples offers multiple health benefits. They're rich in fiber and antioxidants.</p>
					<NavLink to={`/products/6423195430650a6f3a27c835`}>
					<button className={style.btn}>BUY NOW</button>
					</NavLink>
				</div>
			</div>*/}
			
			<div className={style.card}>
				<img src={orange} className={style.cardImg}/>
				<div className={style.Body}>
					<h1 className={style.Title}>ORANGE</h1>
					<p className={style.sub}>₱ 20</p>
					<p className={style.info}>Oranges can keep blood sugar levels in check and reduce high cholesterol to prevent diseases.</p>
					<NavLink to={`/products/64215da6e5309f6e3ad6c890`}>
					<button className={style.btn}>BUY NOW</button>
					</NavLink>
				</div>
			</div>
			
			<div className={style.card}>
				<img src={banana} className={style.cardImg}/>
				<div className={style.Body}>
					<h1 className={style.Title}>BANANA</h1>
					<p className={style.sub}>₱ 5</p>
					<p className={style.info}>Bananas contain essential nutrients that may enhance heart health, help manage blood pressure.</p>
					<NavLink to={`/products/641f86ebbcce204df8140bab`}>
					<button className={style.btn}>BUY NOW</button>
					</NavLink>
				</div>
			</div>

			<div className={style.card}>
				<img src={kiwi} className={style.cardImg}/>
				<div className={style.Body}>
					<h1 className={style.Title}>KIWI</h1>
					<p className={style.sub}>₱ 30</p>
					<p className={style.info}>Kiwifruit are high in vitamin C and contain nutrients, notably nutritionally relevant levels of dietary fibre.</p>
					<NavLink to={`/products/64215f08e5309f6e3ad6c8ab`}>
					<button className={style.btn}>BUY NOW</button>
					</NavLink>
				</div>
			</div>
			
			<div className={style.card}>
				<img src={pineapple} className={style.cardImg}/>
				<div className={style.Body}>
					<h1 className={style.Title}>PINEAPPLE</h1>
					<p className={style.sub}>₱ 40</p>
					<p className={style.info}>Pineapple contains plenty of nutrients and beneficial compounds, such as vitamin C and manganese.</p>
					<NavLink to={`/products/64231a0330650a6f3a27c84b`}>
					<button className={style.btn}>BUY NOW</button>
					</NavLink>
				</div>
			</div>
			
		</div>
	</div>
	
	)
}