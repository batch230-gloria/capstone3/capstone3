import {Navigate, NavLink} from "react-router-dom";
import style from './modules/Footer.module.css';


export default function Footer() {

	return(
	
		<div className={style.container}>
			<div className={style.footer}>
				<div className={style.content}>
					<h3 className={style.h3}>Let's Connect</h3>
					<div className={style.text}>stay updated with our news letter</div>
					<form className={style.form}>
						<input className={style.input} type="email" placeholder="enter your email"/>
						<button className={style.button}><i class='bx bxs-paper-plane'></i></button>
					</form>
					<div className={style.social}>
						<div className={style.bx} ><i  class='bx bxl-facebook-circle' ></i></div>
						<div className={style.bx} ><i  class='bx bxl-twitter' ></i></div>
						<div className={style.bx} ><i  class='bx bxl-instagram' ></i></div>
					</div>
				</div>
				<div className={style.content2}>
					<div className={style.p2}>Copyright & copy 2023</div>

				</div>
			</div>
		</div>
	)

}
