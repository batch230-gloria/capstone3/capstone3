import {useContext, useEffect, useState} from "react";

import { Button, Table, Modal, Form } from "react-bootstrap";
import {Navigate, NavLink} from "react-router-dom";
import Swal from "sweetalert2";
import style from './modules/UserDashboard.module.css';
import UserContext from "../UserContext";

export default function UserDashboard(){

	// const { user } = useContext(UserContext);
	const [userRole, setUserRole] = useState(localStorage.getItem("userRole"));
	
	// Create allusers state to contain the users from the response of our fetch data.
	const [allUsers, setAllUsers] = useState([]);

	const fetchData = () =>{
		// get all the users from the database
		fetch(`${process.env.REACT_APP_API_URL}/users/allUsers`, {
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {


			setAllUsers(data.map(user => {
				return (
					<tr key={user._id} className={style.tr}>
						<td className={style.td}>{user._id}</td>
						<td className={style.td}>{user.firstName}</td>
						<td className={style.td}>{user.lastName}</td>
						<td className={style.td}>{user.email}</td>
						<td className={style.td}>{user.mobileNo}</td>
						<td className={style.td}>{user.isAdmin ? "Admin" : "User"}</td>
						<td className={style.td}>
							{
							(user.isAdmin)
							?
								<>
								<button className={style.button2} onClick={() => toUser(user._id, user.firstName)}><i class="fa-solid fa-file-pen"></i></button>
								<button className={style.button3} onClick={() => deleteUser(user._id)}><i class="fa-solid fa-trash"></i></button>
								</>
							:
								<>
								<button className={style.button2} onClick={() => toAdmin(user._id, user.firstName)}><i class="fa-solid fa-file-pen"></i></button>
								<button className={style.button3} onClick={() => deleteUser(user._id)}><i class="fa-solid fa-trash"></i></button>
								</>
							}
						</td>
					</tr>	
		
				)
			}));
		});
	}

	// to fetch all user in the first render of the page.
	useEffect(()=>{
		fetchData();
	}, [])


	// [SECTION] Setting the user to Admin
	const toUser = (id, userName) =>{
		console.log(id);
		console.log(userName);

		// Using the fetch method to set the isActive property of the user document to false
		fetch(`http://localhost:3005/users/updateUser/${id}`, {
			method: "PATCH",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				isAdmin: false
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data){
				Swal.fire({
					title: "Update Successful",
					icon: "success",
					text: `${userName} is now a user!`
				});
				// To show the update with the specific operation intiated.
				fetchData();
			}
			else{
				Swal.fire({
					title: "Update unsuccessful",
					icon: "error",
					text: "Something went wrong. Please try again later!"
				});
			}
		})
	}


	// Making the user to user
	const toAdmin = (id, userName) =>{
		console.log(id);
		console.log(userName);

		fetch(`http://localhost:3005/users/updateUser/${id}`, {
			method: "PATCH",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				isAdmin: true
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data){
				Swal.fire({
					title: "Update Successful",
					icon: "success",
					text: `${userName} is now a admin!`
				});
				// To show the update with the specific operation intiated.
				fetchData();
			}
			else{
				Swal.fire({
					title: "Update Unsuccessful",
					icon: "error",
					text: "Something went wrong. Please try again later!"
				});
			}
		})
	}

	// [SECTION] Delete User
	const deleteUser = (id) =>{


		fetch(`http://localhost:3005/users/delete/${id}`, {
			method: "DELETE",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
			
		})
		.then(res => res.text())
		.then(data => {
			console.log(data)

			if(data){
				Swal.fire({
					title: "Delete Successful",
					icon: "success",
					text: `User is now deleted.`
				});

				fetchData();
			}
			else{
				Swal.fire({
					title: "Delete unsuccessful",
					icon: "error",
					text: "Something went wrong. Please try again later!"
				});
			}
		})
	}


		return(
			(userRole)
			?
			<>
			
				<div className={style.Icon}>
					<h1>Admin Dashboard</h1>
					{/*Adding a new user */}
					<NavLink to="/admin">
					<button className={style.button4}><i class="fa-solid fa-house"></i></button>
					</NavLink>

					<NavLink to="/userDashboard">
					<button className={style.button4}><i class="fa-solid fa-users"></i></button>
					</NavLink>
					
					<NavLink to="/orderDashboard">
					<button className={style.button5}><i class="fa-solid fa-truck"></i></button>
					</NavLink>
					
				</div>



				<div className={style.tableContainer}>
					<div className={style.tableHeader}>
						<p className={style.text}>User Details</p>
						<div>
							<input className={style.input} placeholder="user" />
							{/*<button className={style.addNew} onClick={openAdd}>+ Add New</button>*/}
						</div>
					</div>
					<div className={style.tableSection}>
						<table className={style.table}>
							<thead>
								<tr>
									<th className={style.th}>Id No.</th>
									<th className={style.th}>First Name</th>
									<th className={style.th}>Last Name</th>
									<th className={style.th}>Email</th>
									<th className={style.th}>Stocks</th>
									<th className={style.th}>Mobile No.</th>
									<th className={style.th}>Action</th>
								</tr>	
							</thead>
							<tbody>
								{allUsers}
							</tbody>
						</table>
					</div>
				</div>

			</>
			:
			<Navigate to="/login" />
		)



}