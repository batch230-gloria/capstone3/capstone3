import {NavLink, Link} from 'react-router-dom';
import style from './modules/LandingPage.module.css';
import "./modules/Navbar.css";
import UserContext from "../UserContext";
import {useState, Fragment, useContext} from 'react';
import "../components/modules/Navbar.css";

export default function LandingPage() {

  const [isMobile, setIsMobile] = useState(false);
  const [userId, setUserId] = useState(localStorage.getItem("userId"));

  const { user, setUser } = useContext(UserContext);


  return(
    <div className={style.HeroContainer}>

      <nav className="navbar">
        <Link to="/" className="home">
        <h3 className="logo">Logo</h3>
        </Link>
        <ul className={isMobile? "nav-links-mobile" : "nav-links"}
        onClick={() => setIsMobile(false)}
        >
          <Link to="/" className="home">
            <li>Home</li>
          </Link>

          <Link to="/contacts" className="products">
            <li>Contact</li>
          </Link>

          <Link to="/products" className="products">
            <li>Products</li>
          </Link>

          {(user.id === null && userId === null)?
          <>
          <Link to="/register" className="products">
            <li>Sign Up</li>
          </Link>

          <Link to="/login" className="signup">
            <li>Sign In</li>
          </Link>
          </>
          :
          <>
          <Link to="/order" className="products">
            <li>Orders</li>
          </Link>

          <Link to="/logout" className="signup">
            <li>Sign Out</li>
          </Link>
          </>
          }

          
        </ul>
        <button 
        className="mobile-menu-icon"
        onClick={() => setIsMobile(!isMobile)}
        >
          {isMobile ? 
          <i className="fas fa-times"></i> 
          : 
          <i className="fas fa-bars"></i>}
        </button>
      </nav>

      <div className={style.HeroContent}>
        <div className={style.HeroItems}>
          <div className={style.HeroH1}>Eat fruits every season to live next season</div>
          <div className={style.HeroP}>Get the fruit fresh look</div>
          <NavLink to="/products">
          <button className={style.HeroBtn}>Explore Now!</button>
          </NavLink>
        </div>
      </div>
    </div>
  )
}