import React from 'react';
import {Link} from "react-router-dom";
import "./modules/Navbar.css";
import UserContext from "../UserContext";
import {useState, Fragment, useContext} from 'react';

export default function Navbar() {

	const [isMobile, setIsMobile] = useState(false);

	const { user, setUser } = useContext(UserContext);

	const [userId, setUserId] = useState(localStorage.getItem("userId"));

	return(
		<nav className="navbar">
			<Link to="/" className="home">
			<h3 className="logo">Logo</h3>
			</Link>
			<ul className={isMobile? "nav-links-mobile" : "nav-links"}
			onClick={() => setIsMobile(false)}
			>
				<Link to="/" className="home">
					<li>Home</li>
				</Link>

				<Link to="/contacts" className="products">
					<li>Contact</li>
				</Link>

				<Link to="/products" className="products">
					<li>Products</li>
				</Link>

				{(user.id === null && userId === null)?
				<>
				<Link to="/register" className="products">
					<li>Sign Up</li>
				</Link>

				<Link to="/login" className="signup">
					<button>Sign In</button>
				</Link>
				</>
				:
				<>
				<Link to="/order" className="products">
					<li>Orders</li>
				</Link>

				<Link to="/logout" className="signup">
					<li>Sign Out</li>
				</Link>
				</>
				}

				
			</ul>
			<button 
			className="mobile-menu-icon"
			onClick={() => setIsMobile(!isMobile)}
			>
				{isMobile ? 
				<i className="fas fa-times"></i> 
				: 
				<i className="fas fa-bars"></i>}
			</button>
		</nav>
	)
}