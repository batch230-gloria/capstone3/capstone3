import orange1 from './images/orange1.jpg';
import pome from './images/pome.jpg';
import blue from './images/blue.jpg';
import green from './images/green.jpg';
import papaya from './images/papaya.jpg';
import style from './modules/About.module.css';


export default function About(){

	return(
		<div className={style.mainContainer}>
			<div className={style.mid}>
				<div className={style.container}>
					<img className={style.bg} src={orange1}/>
					<div className={style.info}>
						<h1 className={style.title}>The freshest, highest quality fruit in <span>affordable</span> price</h1>
						<p className={style.desc}>At Fresh Fruits, we believe customer satisfaction begins with the earth, the land from which the grower creates fine brands and fine produce that are key to our way of doing business.</p>
					</div>
				</div>
			</div>
			
			{/*<div className={style.last}>
				<div className={style.collage}>
					<div className={style.collection}>
						<img src={pome} className={style.img1}/>
						<img src={blue} className={style.img2}/>
						<img src={green} className={style.img3}/>
						<img src={papaya} className={style.img4}/>
						
					</div>
				</div>
			</div>*/}
		</div>	
	)
}

